import {Navbar, NavDropdown, Nav, Container, FormControl, Form, Button} from 'react-bootstrap'
import {CardGroup, Card} from 'react-bootstrap'
import {Row, Col} from "react-bootstrap";
import data from './products.json'
import kat from './categories'
import 'bootstrap/dist/css/bootstrap.min.css';
import {useState} from "react";

function Boot() {
    const [value, setValue] = useState('')
    const [prod, setProd] = useState('')
    const [skidka, setSkidka] = useState('')
    const [vse, setVse] = useState('vse')


    const src = () => {
        setProd(value)
        if (!value && skidka === 'discount'){
            setSkidka('discount')
        }else if (value && skidka === 'all') {
            setVse('s')
        }else if (!value && skidka === 'discount' || skidka === 'all') {
            setVse('vse')
        }
    }
    const z = data.filter(q => {
       if(vse !== 'vse'){
           if (skidka === 'discount') {
               return q.discount != null &&  q.title.toLowerCase().includes(prod.toLowerCase())
           }else if (skidka.id) {
               return skidka.id === q.category_id && q.title.toLowerCase().includes(prod.toLowerCase())
           }else{
               return q.title.toLowerCase().includes(prod.toLowerCase())
           }
       }else{
           if (skidka === 'discount') {
               return q.discount != null &&  q.title.toLowerCase().includes(prod.toLowerCase())
           }else if (skidka.id) {
               return skidka.id === q.category_id && q.title.toLowerCase().includes(prod.toLowerCase())
           }else{
               return q.title.toLowerCase().includes(prod.toLowerCase())
       }
    }})

    const discount = () => {
        setSkidka('discount')
        setValue('')
        setProd('')
    }
    const glav = () => {
        setSkidka('all')
        setVse('vse')
        setValue('')
        setProd('')
    }
    const del = (i) =>{
        setSkidka({id: kat[i].id, title: kat[i].title})
        setValue('')
        setProd('')
    }

    return (
        <>
            <div style={{width: '100%'}} className={'fixed'}>
                <Navbar bg="black" variant="dark" expand="lg">
                    <Container fluid>
                        <Navbar.Brand href="#">ProMarket</Navbar.Brand>
                        <Navbar.Toggle aria-controls="navbarScroll"/>
                        <Navbar.Collapse id="navbarScroll">
                            <Nav
                                className="me-auto my-2 my-lg-0"
                                style={{maxHeight: '100px'}}
                                navbarScroll
                            >
                                <Nav.Link href="#action1" onClick={glav}>Главная</Nav.Link>
                                <Nav.Link href="#action2" onClick={discount}>Скидки</Nav.Link>
                                <NavDropdown title="Категории" id="navbarScrollingDropdown">
                                    {kat.map((q,i) => <NavDropdown.Item href="#action3" onClick={() => del(i)}>{q.short_title}</NavDropdown.Item>)}
                                </NavDropdown>
                            </Nav>
                            <Form className="d-flex">
                                <FormControl
                                    type="search"
                                    placeholder="Что ищете?"
                                    value={value}
                                    onChange={(event) => setValue(event.target.value)}
                                    className="me-2"
                                    aria-label="Search"
                                />
                                <Button variant="outline-success" onClick={src}>Найти</Button>
                            </Form>
                        </Navbar.Collapse>
                    </Container>
                </Navbar>
            </div>
            <div>
                {vse !== 'vse' ? <h1 style={{marginTop: '80px',marginLeft:'40px'}}>{skidka === 'discount' ? 'skidki':z.length === 0 ? 'Результаты поиска: (0)':skidka === 'all' && z.length > 0 ? `Результаты поиска: (${z.length})`:skidka.title}</h1>
                : null}
            </div>
            <div style={{marginTop: '40px'}}>
                <CardGroup>
                    <Container>
                        <Row>
                            {z.map(q => {
                                return (
                                    <Col md={3} className={'mt-4'}>
                                        <Card style={{height: '550px'}} className={'top'}>
                                            <div className={'cont'}>
                                                {q.discount != null ? <h1 className={'percent'}>{q.discount}%</h1> : ''}
                                                <div style={{
                                                    width: '250px',
                                                    height: '300px',
                                                    backgroundImage: `url(https://api.office.promarket.besoft.kg/${q.main_image.path.original})`,
                                                    backgroundSize: 'contain',
                                                    backgroundPosition: 'center',
                                                    backgroundRepeat: 'no-repeat'
                                                }}/>
                                            </div>
                                            <hr color={'lightgrey'} style={{height: '2px', width: '100%'}}/>
                                            <Card.Body className={'display'}>
                                                <Card.Text>
                                                    {q.title}
                                                </Card.Text>
                                                {q.discount === null ?
                                                    <Button variant={"success"}>{q.price}</Button> :
                                                    <Button variant={"success"}
                                                            size={"lg"}>{q.price - (q.price / 100 * q.discount) + ' сом'}
                                                        <span className={'text'}>({q.price})</span></Button>
                                                }
                                            </Card.Body>
                                        </Card>
                                    </Col>
                                )
                            })}
                        </Row>
                    </Container>
                </CardGroup>
            </div>
        </>
    )

}

export default Boot